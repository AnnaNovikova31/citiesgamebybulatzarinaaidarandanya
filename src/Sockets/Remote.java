package Sockets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class Remote {
    private Socket socket;

    public void initServer() throws IOException {
        ServerSocket serverSocket = new ServerSocket(1234);
        socket = serverSocket.accept();
    }

    public void connect(String address) throws IOException{
        Socket socket = new Socket(InetAddress.getByName(address), 1234);
    }

    public void send(String string) throws IOException {
        DataOutputStream out1 = new DataOutputStream(socket.getOutputStream());
        out1.write(string.getBytes());
    }

    public String receive() throws IOException {
        DataInputStream in1 = new DataInputStream(socket.getInputStream());
        System.out.println("DataInputStream created");
        return in1.readUTF();
    }

}

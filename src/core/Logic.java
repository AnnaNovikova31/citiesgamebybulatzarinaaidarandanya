package core;


import IO.Reader;
import Sockets.Remote;


import java.io.IOException;

import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

public class Logic {
    static Set<String> lastWords;
    static Set<String> allCities;
    static String latestWord;
    static Remote remote;
    static boolean endGame = false;
    public static void main(String[] args) throws IOException {
        allCities = Reader.readLines();
        System.out.println("Привет!");
        System.out.println("Это игра в города.");
        System.out.println("Для того чтобы играть, тебе необходимо знать свой ip-адрес или ip-адрес друга");
        System.out.println("В любой момент ты можешь прекратить игру, введя 0");
        remote = new Remote();

        String answer;
        Scanner in = new Scanner(System.in);
        boolean commandAccepted = false;
        while (!commandAccepted) {
            System.out.println("Хочешь ли ты стать сервером или подсоединиться к уже существующему?1/2");
            answer = in.nextLine();
            if (answer.equals("1")) {
                commandAccepted = true;
                remote.initServer();
            }else {
                if (answer.equals("2")){
                    commandAccepted = true;
                    System.out.println("Введи ip адрес друга");
                    answer = in.nextLine();
                    remote.connect(answer);
                }else {
                    if (answer.equals("0")) {
                        endGame = true;
                        break;
                    }else {
                        System.out.println("Невозможно распознать команду. Попробуй еще раз");
                    }
                }

            }
        }

        while (!endGame) {
            System.out.println("Введите город ");
            answer = in.nextLine().toUpperCase();
            if (answer.equals("0")) {
                endGame = true;
                break;
            }
            boolean isAllRight = true;
            if (! lastWords.add(answer)) {
                System.out.println("Такой город уже назван!");
                isAllRight = false;
            }
            if(isAllRight && !allCities.contains(answer)) {
                System.out.println("Такого города нет в нашей базе российских городов. Попробуйте снова");
                isAllRight = false;
            }
            if (isAllRight && latestWord!= null) {
                if (answer.charAt(0) != latestWord.charAt(answer.length() - 1)) {
                    System.out.println("Город должен начинаться на ту же букву, на которую оканчивается город соперника");
                    isAllRight = false;
                }
            }
            if (isAllRight) {
                latestWord = answer;
                remote.send(latestWord);
                String s = remote.receive();
                if (s.equals("0")) {
                    System.out.println("Соперник вышел из игры");
                    endGame = true;
                } else {
                    System.out.println("Соперник назвал город " + s);
                }
            }
        }
        if (endGame) {
            System.out.println("Игра завершена.");
            remote.send("0");
        }

    }


}

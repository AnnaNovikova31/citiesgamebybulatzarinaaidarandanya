package IO;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class Reader {
    public static Set<String> readLines() {
        Set<String> result = new HashSet<>();
        try {
            Scanner in = new Scanner(new File("cities.txt"));
            while (in.hasNext()) {
                result.add(in.nextLine().toUpperCase());
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
